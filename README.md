# RenderingCourse18

Repository for materials presented in the graphics & rendering course under the umbrella of Coherent Labs in Sept '18.  
Lecturers: Nikola Vasilev / Alexander Angelov

Lectures:  
01/ Intro: [Slides](https://drive.google.com/open?id=1MBwm0aPd4ttkSicrTpgs5qEoSPJIatZxS5EUtw31lUk) / [Video](https://youtu.be/pa8h_spivug)  
02/ Graphics pipeline: [Slides](https://drive.google.com/open?id=10mR6EhYelAeExN5-Ur2GYcfYDsvYeZCfmACBUmjfu9k) / [Video](https://youtu.be/FHBzSQrOYUw)  
03/ Graphics APIs: [Slides](https://docs.google.com/presentation/d/1IPDxs7oj6QXA9pWPHMKXR4RH8Qx00RL1r8JcP7zOyFU/) / [Video](https://youtu.be/nhCzUEFb8Aw)  
04/ 2D Graphics & Compute: [Slides](https://docs.google.com/presentation/d/1ys1_5iopbOVn2tjpRRefyycWRBEqCITL7HcxqjvuUv0) / [Video](https://youtu.be/8ffdnD59vsU)  
05/ 3D Graphics (Part 1): [Slides](https://docs.google.com/presentation/d/1ENt6dJ9UjPwUPou3f0x7k7c5GyZ2z5Gj1pBRiWk7HD8) / [Video](https://youtu.be/PmiVV5NUAmI)  
06/ 3D Graphics (Part 2): [Slides](https://docs.google.com/presentation/d/1mrEfwhvpzX0Z209pYMJqjkOX06O18eju0bLICxk8HGs) / [Video](https://youtu.be/avkou8ptMnE)  
07/ Debugging & Profiling: [Slides](https://docs.google.com/presentation/d/1jbzr_-NpQcKHTwKqy92EALDpBQSSY5q1pvm-Uy4Gk1k) / [Video](https://youtu.be/Xf5p1HnSSBA)  
08/ Raytracing: [Slides](https://docs.google.com/presentation/d/1dhjjUOXYHcEL7KChOmYeGAUpzqVAGyufYXYqoCOKNF0) / [Video](https://youtu.be/EjNqIC7qqOM)  
09/ Case Study: Frame Analysis [Video](https://youtu.be/7eiUPaKgRzw)  
10/ Bonus: Antialiasing, Shadow mapping techniques [Video](https://youtu.be/CD8T96SwXzE)  