#include "stdafx.h"

#include "Render.h"

template <typename T>
void SafeRelease(T* x)
{
	if (x)
	{
		x->Release();
	}
}

Renderer::Renderer()
	: m_Device(nullptr)
	, m_ImmediateContext(nullptr)
	, m_SwapChain(nullptr)
	, m_BackBufferView(nullptr)
	, m_BackDepthStencilView(nullptr)
	, m_DefaultDepthStencilState(nullptr)
	, m_LinearSampler(nullptr)
	, m_VS(nullptr)
	, m_PS(nullptr)
{}

Renderer::~Renderer()
{
	SafeRelease(m_VS);
	SafeRelease(m_PS);

	SafeRelease(m_Texture.TextureRTV);
	SafeRelease(m_Texture.TextureSRV);
	SafeRelease(m_Texture.Texture);

	SafeRelease(m_LinearSampler);
	SafeRelease(m_SwapChain);
	SafeRelease(m_BackBufferView);
	SafeRelease(m_BackDepthStencilView);
	SafeRelease(m_ImmediateContext);
	SafeRelease(m_Device);
	SafeRelease(m_DepthStencil.DSV);
	SafeRelease(m_DepthStencil.Texture);
	SafeRelease(m_DefaultDepthStencilState);
}

bool Renderer::MakeTexture(int width, int height, DXGI_FORMAT format, bool isRT, bool isShared)
{
	m_Texture.Texture = nullptr;
	m_Texture.TextureSRV = nullptr;
	m_Texture.TextureRTV = nullptr;

	D3D11_TEXTURE2D_DESC desc;
	::ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = format;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	if(isRT)
	{
		desc.BindFlags |= D3D11_BIND_RENDER_TARGET;
	}
	desc.MiscFlags = isShared ? D3D11_RESOURCE_MISC_SHARED : 0;

	ID3D11Texture2D* tex = nullptr;
	HRESULT hr = m_Device->CreateTexture2D(&desc, nullptr, &tex);
	if (FAILED(hr))
	{
		SafeRelease(tex);
		return false;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc = CD3D11_SHADER_RESOURCE_VIEW_DESC(tex, D3D11_SRV_DIMENSION_TEXTURE2D);
	ID3D11ShaderResourceView* srv = nullptr;
	hr = m_Device->CreateShaderResourceView(tex, &srDesc, &srv);

	if (FAILED(hr))
	{
		SafeRelease(tex);
		return false;
	}

	if (isRT)
	{
		D3D11_RENDER_TARGET_VIEW_DESC rtDesc;
		::ZeroMemory(&rtDesc, sizeof(rtDesc));
		rtDesc.Format = desc.Format;
		rtDesc.Texture2D.MipSlice = 0;
		rtDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;

		ID3D11RenderTargetView *rtv = nullptr;
		hr = m_Device->CreateRenderTargetView(tex, &rtDesc, &rtv);
		if (FAILED(hr))
		{
			SafeRelease(srv);
			SafeRelease(tex);
			return false;
		}

		m_Texture.TextureRTV = rtv;
	}

	m_Texture.Format = format;
	m_Texture.Width = width;
	m_Texture.Height = height;
	m_Texture.HasMips = 1;
	m_Texture.IsDynamic = false;
	m_Texture.IsRenderTarget = isRT;
	m_Texture.Texture = tex;
	m_Texture.TextureSRV = srv;
	m_Texture.SampleCount = 1;

	return true;
}

bool Renderer::MakeDepthStencil(int width, int height, int sampleCount)
{
	m_DepthStencil.Texture = nullptr;
	m_DepthStencil.DSV = nullptr;

	D3D11_TEXTURE2D_DESC descDepth;
	::ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = sampleCount;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	ID3D11Texture2D *texture;
	if (FAILED(m_Device->CreateTexture2D(&descDepth,
		nullptr,
		&texture)))
	{
		return false;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	::ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDSV.ViewDimension = (sampleCount == 1) ? D3D11_DSV_DIMENSION_TEXTURE2D : D3D11_DSV_DIMENSION_TEXTURE2DMS;
	descDSV.Texture2D.MipSlice = 0;

	ID3D11DepthStencilView *dsv;
	if (FAILED(m_Device->CreateDepthStencilView(
		texture,
		&descDSV,
		&dsv)))
	{
		SafeRelease(texture);
		SafeRelease(dsv);
		return false;
	}

	m_DepthStencil.Texture = texture;
	m_DepthStencil.DSV = dsv;
	m_DepthStencil.Width = width;
	m_DepthStencil.Height = height;

	return true;
}

bool Renderer::Initialize(HWND window, int width, int height)
{
	m_Width = width;
	m_Height = height;

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	unsigned numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	unsigned numFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC sd;
	::ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = window;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	HRESULT hr = S_OK;
	D3D_FEATURE_LEVEL featureLevel;
	for(unsigned driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
	{
		D3D_DRIVER_TYPE driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(nullptr, driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &m_SwapChain, &m_Device, &featureLevel, &m_ImmediateContext);
		if(SUCCEEDED(hr))
		{
			break;
		}
	}
	if(FAILED(hr)) return false;

	// Create a render target view
	ID3D11Texture2D* pBackBuffer = nullptr;
	hr = m_SwapChain->GetBuffer(0, __uuidof( ID3D11Texture2D ), (LPVOID*)&pBackBuffer);
	if(FAILED(hr)) return false;

	hr = m_Device->CreateRenderTargetView(pBackBuffer, nullptr, &m_BackBufferView);
	pBackBuffer->Release();
	if(FAILED(hr)) return false;

	m_ImmediateContext->OMSetRenderTargets(1, &m_BackBufferView, m_BackDepthStencilView);
	m_ImmediateContext->OMGetDepthStencilState(&m_DefaultDepthStencilState, nullptr);

	// Setup the viewport
	D3D11_VIEWPORT vp;
	vp.Width = (float)m_Width;
	vp.Height = (float)m_Height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	m_ImmediateContext->RSSetViewports(1, &vp);

	// Make a sampler
	D3D11_SAMPLER_DESC sampDesc = CD3D11_SAMPLER_DESC(CD3D11_DEFAULT());
	hr = m_Device->CreateSamplerState(&sampDesc, &m_LinearSampler);
	if(FAILED(hr)) return false;

	// Create shaders
	ID3DBlob* compiledShader = nullptr;
	ID3DBlob* errors = nullptr;
	hr = D3DCompileFromFile(L"HelloGTDx11.fx", nullptr, nullptr, "VS", "vs_4_0", 0, 0, &compiledShader, &errors);
	SafeRelease(errors);
	if(FAILED(hr)) return false;

	hr = m_Device->CreateVertexShader(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), nullptr, &m_VS);
	SafeRelease(compiledShader);
	if(FAILED(hr)) return false;

	hr = D3DCompileFromFile(L"HelloGTDx11.fx", nullptr, nullptr, "PS", "ps_4_0", 0, 0, &compiledShader, &errors);
	SafeRelease(errors);
	if(FAILED(hr)) return false;

	hr = m_Device->CreatePixelShader(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), nullptr, &m_PS);
	SafeRelease(compiledShader);
	if(FAILED(hr)) return false;

	// Make a texture to hold the UI
	if (!MakeTexture(m_Width, m_Height, DXGI_FORMAT_R8G8B8A8_UNORM, true)) return false;

	if (!MakeDepthStencil(m_Width, m_Height, 1)) return false;

	// We'll just draw a full-screen triangle, so set everything here
	m_ImmediateContext->IASetInputLayout(nullptr);
	m_ImmediateContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);
	m_ImmediateContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	m_ImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_ImmediateContext->VSSetShader(m_VS, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_PS, nullptr, 0);
	m_ImmediateContext->PSSetSamplers(0, 1, &m_LinearSampler);

	return true;
}

void ExtractColors(unsigned color, float output[4])
{
	for (auto i = 0; i < 4; ++i)
	{
		output[i] = (color & 0xFF) / 255.0f;
		color >>= 8;
	}
}

void Renderer::DrawScene()
{

	m_ImmediateContext->OMSetDepthStencilState(m_DefaultDepthStencilState, 0u);
	m_ImmediateContext->OMSetRenderTargets(1, &m_BackBufferView, m_BackDepthStencilView);

	float clearColor[4];
	ExtractColors(0xFF000000, clearColor);

	D3D11_RECT r;
	r.top = 0;
	r.left = 0;
	r.right = m_Width;
	r.bottom = m_Height;

	m_ImmediateContext->RSSetScissorRects(1, &r);
	m_ImmediateContext->ClearRenderTargetView(m_BackBufferView, clearColor);
	m_ImmediateContext->VSSetShader(m_VS, nullptr, 0);
	m_ImmediateContext->PSSetShader(m_PS, nullptr, 0);
	m_ImmediateContext->PSSetShaderResources(0, 1, &(m_Texture.TextureSRV));
	m_ImmediateContext->PSSetSamplers(0, 1, &m_LinearSampler);
	m_ImmediateContext->Draw(3, 0);

	ID3D11ShaderResourceView* nullSrv = {nullptr};
	m_ImmediateContext->PSSetShaderResources(0, 1, &nullSrv);

	m_SwapChain->Present(1, 0);
}